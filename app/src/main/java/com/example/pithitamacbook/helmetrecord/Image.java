package com.example.pithitamacbook.helmetrecord;

/**
 * Created by pithitamacbook on 5/9/2017 AD.
 */

public class Image {
    private String title;
    int image;

    public Image(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public int getImage() {
        return image;
    }

}
