package com.example.pithitamacbook.helmetrecord;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by pithitamacbook on 5/10/2017 AD.
 */

public class ImageAdapter extends
        RecyclerView.Adapter<ImageAdapter.MyViewHolder> {

    private List<Image> imageList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);

        }
    }

    public ImageAdapter(List<Image> imageList) {
        this.imageList = imageList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycleview, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,final int position) {
        Image image = imageList.get(position);
        holder.title.setText(image.getTitle());

    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }
}